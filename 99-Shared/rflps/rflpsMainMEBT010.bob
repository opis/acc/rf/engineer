<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-08-19 14:51:14 by rafaelmontano-->
<display version="2.0.0">
  <name>rflpsMainMEBT</name>
  <width>2280</width>
  <height>1200</height>
  <background_color>
    <color name="Transparent" red="255" green="255" blue="255" alpha="0">
    </color>
  </background_color>
  <actions>
  </actions>
  <widget type="rectangle" version="2.0.0">
    <name>MGGrey03-background_3</name>
    <x>1270</x>
    <width>620</width>
    <height>60</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>MGGrey03-background_5</name>
    <x>1900</x>
    <width>374</width>
    <height>60</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="tabs" version="2.0.0">
    <name>Tabbed Container</name>
    <macros>
      <IOC_>$(R_FIM_)</IOC_>
    </macros>
    <tabs>
      <tab>
        <name>SSPA Overview</name>
        <children>
          <widget type="embedded" version="2.0.0">
            <name>Embedded Display_1</name>
            <macros>
              <PREFIX>$(P)$(R_SSPA)</PREFIX>
              <PSUB1>IM1</PSUB1>
              <PSUB2>IM2</PSUB2>
              <PSUB3>IM3</PSUB3>
              <PSUB4>IM4</PSUB4>
              <PSUB5>IM5</PSUB5>
            </macros>
            <file>../mebt-buncher/BTESA_AMP_RF_General.bob</file>
            <x>24</x>
            <y>20</y>
            <width>1690</width>
            <height>1040</height>
          </widget>
        </children>
      </tab>
      <tab>
        <name>FIM Transitions</name>
        <children>
          <widget type="embedded" version="2.0.0">
            <name>Embedded Display_2</name>
            <macros>
              <IOC_>$(R_FIM_)</IOC_>
            </macros>
            <file>support/transitions_fim_MEBT.bob</file>
            <width>1970</width>
            <height>1090</height>
            <resize>2</resize>
          </widget>
        </children>
      </tab>
      <tab>
        <name>Diagnostics</name>
        <children>
          <widget type="embedded" version="2.0.0">
            <name>Embedded Display</name>
            <macros>
              <IOCSTATS_FIM>$(IOC_FIM)</IOCSTATS_FIM>
              <IOCSTATS_SIM>$(IOC_SSPA)</IOCSTATS_SIM>
              <IOC_>$(R_FIM)</IOC_>
            </macros>
            <file>support/fimDiagnostics.bob</file>
            <x>14</x>
            <y>19</y>
            <width>860</width>
            <height>800</height>
            <resize>2</resize>
          </widget>
          <widget type="group" version="3.0.0">
            <name>SSPA_FSM</name>
            <x>915</x>
            <y>17</y>
            <width>260</width>
            <height>290</height>
            <style>3</style>
            <transparent>true</transparent>
            <widget type="rectangle" version="2.0.0">
              <name>MGGrey03-background_7</name>
              <width>260</width>
              <height>290</height>
              <line_width>2</line_width>
              <line_color>
                <color name="GROUP-BORDER" red="150" green="155" blue="151">
                </color>
              </line_color>
              <background_color>
                <color name="Background" red="220" green="225" blue="221">
                </color>
              </background_color>
              <corner_width>10</corner_width>
              <corner_height>10</corner_height>
            </widget>
            <widget type="textupdate" version="2.0.0">
              <name>text_up_version_6</name>
              <pv_name>$(P)$(R_SSPA):FSM-RB</pv_name>
              <x>125</x>
              <y>40</y>
              <height>30</height>
              <foreground_color>
                <color red="0" green="0" blue="0">
                </color>
              </foreground_color>
              <background_color>
                <color red="255" green="255" blue="255">
                </color>
              </background_color>
              <horizontal_alignment>1</horizontal_alignment>
              <vertical_alignment>1</vertical_alignment>
              <wrap_words>false</wrap_words>
              <actions>
              </actions>
              <border_alarm_sensitive>false</border_alarm_sensitive>
              <border_color>
                <color red="0" green="128" blue="255">
                </color>
              </border_color>
            </widget>
            <widget type="action_button" version="3.0.0">
              <name>Action Button_4</name>
              <actions>
                <action type="write_pv">
                  <pv_name>$(P)$(R_SSPA):FSM-Cmd</pv_name>
                  <value>3</value>
                  <description>WritePV</description>
                </action>
              </actions>
              <pv_name>$(P)$(R_SSPA):FSM-Cmd</pv_name>
              <text>RF ON</text>
              <x>125</x>
              <y>80</y>
              <height>40</height>
              <foreground_color>
                <color red="0" green="0" blue="0">
                </color>
              </foreground_color>
              <tooltip>$(actions)</tooltip>
              <border_alarm_sensitive>false</border_alarm_sensitive>
            </widget>
            <widget type="action_button" version="3.0.0">
              <name>Action Button_3</name>
              <actions>
                <action type="write_pv">
                  <pv_name>$(P)$(R_SSPA):FSM-Cmd</pv_name>
                  <value>2</value>
                  <description>WritePV</description>
                </action>
              </actions>
              <pv_name>$(P)$(R_SSPA):FSM-Cmd</pv_name>
              <text>RF READY</text>
              <x>125</x>
              <y>130</y>
              <height>40</height>
              <foreground_color>
                <color red="0" green="0" blue="0">
                </color>
              </foreground_color>
              <tooltip>$(actions)</tooltip>
              <border_alarm_sensitive>false</border_alarm_sensitive>
            </widget>
            <widget type="action_button" version="3.0.0">
              <name>Action Button_2</name>
              <actions>
                <action type="write_pv">
                  <pv_name>$(P)$(R_SSPA):FSM-Cmd</pv_name>
                  <value>1</value>
                  <description>WritePV</description>
                </action>
              </actions>
              <pv_name>$(P)$(R_SSPA):FSM-Cmd</pv_name>
              <text>STANDBY</text>
              <x>125</x>
              <y>180</y>
              <height>40</height>
              <foreground_color>
                <color red="0" green="0" blue="0">
                </color>
              </foreground_color>
              <tooltip>$(actions)</tooltip>
              <border_alarm_sensitive>false</border_alarm_sensitive>
            </widget>
            <widget type="action_button" version="3.0.0">
              <name>Action Button_1</name>
              <actions>
                <action type="write_pv">
                  <pv_name>$(P)$(R_SSPA):FSM-Cmd</pv_name>
                  <value>0</value>
                  <description>WritePV</description>
                </action>
              </actions>
              <pv_name>$(P)$(R_SSPA):FSM-Cmd</pv_name>
              <text>RF OFF</text>
              <x>125</x>
              <y>230</y>
              <height>40</height>
              <foreground_color>
                <color red="0" green="0" blue="0">
                </color>
              </foreground_color>
              <tooltip>$(actions)</tooltip>
              <border_alarm_sensitive>false</border_alarm_sensitive>
            </widget>
            <widget type="label" version="2.0.0">
              <name>Label_7</name>
              <text>Current State</text>
              <x>10</x>
              <y>40</y>
              <width>110</width>
              <height>37</height>
              <foreground_color>
                <color red="0" green="0" blue="0">
                </color>
              </foreground_color>
              <background_color>
                <color red="255" green="255" blue="255">
                </color>
              </background_color>
              <horizontal_alignment>2</horizontal_alignment>
              <vertical_alignment>1</vertical_alignment>
              <wrap_words>false</wrap_words>
              <actions>
              </actions>
              <border_color>
                <color red="0" green="128" blue="255">
                </color>
              </border_color>
            </widget>
            <widget type="label" version="2.0.0">
              <name>Label_321</name>
              <text>SSPA Internal State Machine</text>
              <x>10</x>
              <width>240</width>
              <height>40</height>
              <font>
                <font family="Source Sans Pro" style="BOLD" size="16.0">
                </font>
              </font>
              <horizontal_alignment>1</horizontal_alignment>
              <vertical_alignment>1</vertical_alignment>
              <actions>
              </actions>
            </widget>
          </widget>
          <widget type="group" version="3.0.0">
            <name>SSPA_FSM_1</name>
            <x>915</x>
            <y>327</y>
            <width>260</width>
            <height>242</height>
            <style>3</style>
            <transparent>true</transparent>
            <widget type="rectangle" version="2.0.0">
              <name>MGGrey03-background_8</name>
              <width>260</width>
              <height>242</height>
              <line_width>2</line_width>
              <line_color>
                <color name="GROUP-BORDER" red="150" green="155" blue="151">
                </color>
              </line_color>
              <background_color>
                <color name="Background" red="220" green="225" blue="221">
                </color>
              </background_color>
              <corner_width>10</corner_width>
              <corner_height>10</corner_height>
            </widget>
            <widget type="textupdate" version="2.0.0">
              <name>text_up_version_7</name>
              <pv_name>$(P)$(R_FIM)FSM-RB</pv_name>
              <x>125</x>
              <y>40</y>
              <height>30</height>
              <foreground_color>
                <color red="0" green="0" blue="0">
                </color>
              </foreground_color>
              <background_color>
                <color red="255" green="255" blue="255">
                </color>
              </background_color>
              <horizontal_alignment>1</horizontal_alignment>
              <vertical_alignment>1</vertical_alignment>
              <wrap_words>false</wrap_words>
              <actions>
              </actions>
              <border_alarm_sensitive>false</border_alarm_sensitive>
              <border_color>
                <color red="0" green="128" blue="255">
                </color>
              </border_color>
            </widget>
            <widget type="action_button" version="3.0.0">
              <name>Action Button_5</name>
              <actions>
                <action type="write_pv">
                  <pv_name>$(P)$(R_FIM)FSM</pv_name>
                  <value>3</value>
                  <description>WritePV</description>
                </action>
              </actions>
              <pv_name>$(P)$(R_FIM)FSM</pv_name>
              <text>RF ON</text>
              <x>125</x>
              <y>80</y>
              <height>40</height>
              <foreground_color>
                <color red="0" green="0" blue="0">
                </color>
              </foreground_color>
              <tooltip>$(actions)</tooltip>
              <border_alarm_sensitive>false</border_alarm_sensitive>
            </widget>
            <widget type="action_button" version="3.0.0">
              <name>Action Button_6</name>
              <actions>
                <action type="write_pv">
                  <pv_name>$(P)$(R_FIM)FSM</pv_name>
                  <value>2</value>
                  <description>WritePV</description>
                </action>
              </actions>
              <pv_name>$(P)$(R_FIM)FSM</pv_name>
              <text>HV ON</text>
              <x>125</x>
              <y>130</y>
              <height>40</height>
              <foreground_color>
                <color red="0" green="0" blue="0">
                </color>
              </foreground_color>
              <tooltip>$(actions)</tooltip>
              <border_alarm_sensitive>false</border_alarm_sensitive>
            </widget>
            <widget type="action_button" version="3.0.0">
              <name>Action Button_7</name>
              <actions>
                <action type="write_pv">
                  <pv_name>$(P)$(R_FIM)FSM</pv_name>
                  <value>0</value>
                  <description>WritePV</description>
                </action>
              </actions>
              <pv_name>$(P)$(R_FIM)FSM</pv_name>
              <text>IDLE</text>
              <x>125</x>
              <y>180</y>
              <height>40</height>
              <foreground_color>
                <color red="0" green="0" blue="0">
                </color>
              </foreground_color>
              <tooltip>$(actions)</tooltip>
              <border_alarm_sensitive>false</border_alarm_sensitive>
            </widget>
            <widget type="label" version="2.0.0">
              <name>Label_322</name>
              <text>Current State</text>
              <x>10</x>
              <y>40</y>
              <width>110</width>
              <height>37</height>
              <foreground_color>
                <color red="0" green="0" blue="0">
                </color>
              </foreground_color>
              <background_color>
                <color red="255" green="255" blue="255">
                </color>
              </background_color>
              <horizontal_alignment>2</horizontal_alignment>
              <vertical_alignment>1</vertical_alignment>
              <wrap_words>false</wrap_words>
              <actions>
              </actions>
              <border_color>
                <color red="0" green="128" blue="255">
                </color>
              </border_color>
            </widget>
            <widget type="label" version="2.0.0">
              <name>Label_323</name>
              <text>FIM Internal State Machine</text>
              <x>10</x>
              <width>240</width>
              <height>40</height>
              <font>
                <font family="Source Sans Pro" style="BOLD" size="16.0">
                </font>
              </font>
              <horizontal_alignment>1</horizontal_alignment>
              <vertical_alignment>1</vertical_alignment>
              <actions>
              </actions>
            </widget>
          </widget>
        </children>
      </tab>
      <tab>
        <name>   Interlocks</name>
        <children>
          <widget type="embedded" version="2.0.0">
            <name>Embedded Display_1</name>
            <file>support/InterlocksMEBT.bob</file>
            <y>2</y>
            <width>1965</width>
            <height>1085</height>
          </widget>
        </children>
      </tab>
    </tabs>
    <x>300</x>
    <y>60</y>
    <width>1970</width>
    <height>1121</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="21.0">
      </font>
    </font>
    <tab_height>20</tab_height>
    <actions>
    </actions>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_310</name>
    <text>Actual State:</text>
    <x>1270</x>
    <y>15</y>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_311</name>
    <class>SECTION</class>
    <text>SSPA:</text>
    <x>1590</x>
    <y>15</y>
    <width>51</width>
    <height>30</height>
    <font>
      <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <actions>
    </actions>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update_123</name>
    <pv_name>$(P)$(R_SSPA):FSM-RB</pv_name>
    <x>1651</x>
    <y>19</y>
    <width>80</width>
    <height>25</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <actions>
    </actions>
    <tooltip>$(pv_name)
$(pv_value)
Actual Machine State read-back</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_312</name>
    <class>SECTION</class>
    <text>FIM:</text>
    <x>1740</x>
    <y>15</y>
    <width>40</width>
    <height>30</height>
    <font>
      <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <actions>
    </actions>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update_124</name>
    <pv_name>$(P)$(R_FIM)FSM-RB</pv_name>
    <x>1790</x>
    <y>19</y>
    <width>80</width>
    <height>25</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <actions>
    </actions>
    <tooltip>$(pv_name)
$(pv_value)
Actual Machine State read-back</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update_128</name>
    <pv_name>$(P)$(R_SSPA):ResetRequired</pv_name>
    <x>2080</x>
    <y>19</y>
    <width>50</width>
    <height>25</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="alarmRule" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>
            <color name="MINOR" red="252" green="242" blue="17">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
      </rule>
    </rules>
    <tooltip>$(pv_name)
$(pv_value)
One or more signals are being interlocked or may require an acknowledge.</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>RESET_BUTTON_4</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>WritePV</description>
      </action>
    </actions>
    <pv_name>$(P)$(R_SSPA):AlarmReset</pv_name>
    <text>Reset Alarms</text>
    <x>2143</x>
    <y>17</y>
    <width>115</width>
    <background_color>
      <color red="240" green="240" blue="240">
      </color>
    </background_color>
    <tooltip>$(pv_name)
$(pv_value)
Reset all non-active interlocks</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>This will reset all non-active interlocks</confirm_message>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>EmbeddedMachineControl</name>
    <file>support/machineStateControlMEBT.bob</file>
    <width>300</width>
    <height>1180</height>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>MGGrey03-background_6</name>
    <x>800</x>
    <width>220</width>
    <height>60</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_320</name>
    <text>FIM Reset:</text>
    <x>810</x>
    <y>15</y>
    <width>74</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <actions>
    </actions>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>Write PV</description>
      </action>
    </actions>
    <pv_name>$(P)$(R_FIM)Rst</pv_name>
    <text>FIM Reset</text>
    <x>900</x>
    <y>15</y>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>$(P)$(R_SSPA)</text>
    <x>320</x>
    <width>480</width>
    <height>45</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_319</name>
    <text>SSPA reset request</text>
    <x>1910</x>
    <width>160</width>
    <height>60</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <actions>
    </actions>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update_125</name>
    <pv_name>$(P)$(R_SSPA):MainFSM-RB</pv_name>
    <x>1471</x>
    <y>19</y>
    <width>80</width>
    <height>25</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <actions>
    </actions>
    <tooltip>$(pv_name)
$(pv_value)
Actual Machine State read-back</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_313</name>
    <class>SECTION</class>
    <text>Main:</text>
    <x>1420</x>
    <y>15</y>
    <width>40</width>
    <height>30</height>
    <font>
      <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <actions>
    </actions>
  </widget>
</display>
