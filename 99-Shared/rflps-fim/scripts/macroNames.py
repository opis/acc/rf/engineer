from org.csstudio.opibuilder.scriptUtil import PVUtil

# This script will populate several macros that will be used to mount the PV names inside the GUIs
# In this first version, the content of the macros are hardcoded inside this script. 
# The next step is to read the values from a file under version control;

# Section and subsection are selected by the user 
prefix_ = PVUtil.getString(pvs[0])
sec_ = PVUtil.getString(pvs[1])
subsec_ = PVUtil.getString(pvs[2])

# Adding the macros on the widget that will consume this script
widget.getPropertyValue("macros").add("PREFIX", prefix_)
widget.getPropertyValue("macros").add("SEC", sec_)
widget.getPropertyValue("macros").add("SUB", subsec_)


##################################################################################################
#	Hardcoded macro data
##################################################################################################

# This macro refers to the IOC main PVs:
ioc_ = "FIM-110"

# This macro refers to the IOCStats Modules:
iocstats_ = "RFLAB-01"

# Analog input macros
ai00_ = "Mod-110:Cur"
ai01_ = "Mod-110:Vol"
ai02_ = "SolPS-110:Cur"
ai03_ = "SolPS-120:Cur"
ai04_ = "SolPS-130:Cur"
ai05_ = "FIM-110:AI5"
ai06_ = "FIM-110:AI6"
ai07_ = "FIM-110:AI7"
ai08_ = "FIM-110:AI8"
ai09_ = "FIM-110:AI9"
ai10_ = "DirC-110:DaPwrFwdDA"
ai11_ = "DirC-110:PwrFwdKly"
ai12_ = "DirC-110:PwrFwdLoad"
ai13_ = "DirC-110:PwrRefKly"
ai14_ = "FIM-110:AI14"
ai15_ = "FIM-110:AI15"
ai16_ = "FIM-110:AI16"
ai17_ = "DirC-110:PwrRefLoad"
ai18_ = "FIM-110:AI18"
ai19_ = "FIM-110:AI19"

di00_ = "SIM-110:HvEnaCmd"
di01_ = "SIM-110:RfEnaCmd"
di02_ = "SIM-110:PCconnect"
di03_ = "Mod-110:Fault"
di04_ = "VacPS-110:I-SP"
di05_ = "VacPS-110:I-SP"
di06_ = "VacPS-120:I-SP"
di07_ = "FIM-110:DI7"
di08_ = "ADR-201001:ItckStat"
di09_ = "ADR-201002:ItckStat"
di10_ = "ADR-201003:ItckStat"
di11_ = "ADR-201004:ItckStat"
di12_ = "ADR-201005:ItckStat"
di13_ = "FIM-110:DI13"
di14_ = "FIM-110:DI14"
di15_ = "FIM-110:DI15"

########################################################################################################

# Adding the macros on the widget that will consume this script
widget.getPropertyValue("macros").add("IOC_", ioc_)
widget.getPropertyValue("macros").add("IOCSTATS_", iocstats_)


# Analog input macros
widget.getPropertyValue("macros").add("AI00", ai00_)
widget.getPropertyValue("macros").add("AI01", ai01_)
widget.getPropertyValue("macros").add("AI02", ai02_)
widget.getPropertyValue("macros").add("AI03", ai03_)
widget.getPropertyValue("macros").add("AI04", ai04_)
widget.getPropertyValue("macros").add("AI05", ai05_)
widget.getPropertyValue("macros").add("AI06", ai06_)
widget.getPropertyValue("macros").add("AI07", ai07_)
widget.getPropertyValue("macros").add("AI08", ai08_)
widget.getPropertyValue("macros").add("AI09", ai09_)
widget.getPropertyValue("macros").add("AI10", ai10_)
widget.getPropertyValue("macros").add("AI11", ai11_)
widget.getPropertyValue("macros").add("AI12", ai12_)
widget.getPropertyValue("macros").add("AI13", ai13_)
widget.getPropertyValue("macros").add("AI14", ai14_)
widget.getPropertyValue("macros").add("AI15", ai15_)
widget.getPropertyValue("macros").add("AI16", ai16_)
widget.getPropertyValue("macros").add("AI17", ai17_)
widget.getPropertyValue("macros").add("AI18", ai18_)
widget.getPropertyValue("macros").add("AI19", ai19_)

# Digital input macros
widget.getPropertyValue("macros").add("DI00", di00_)
widget.getPropertyValue("macros").add("DI01", di01_)
widget.getPropertyValue("macros").add("DI02", di02_)
widget.getPropertyValue("macros").add("DI03", di03_)
widget.getPropertyValue("macros").add("DI04", di04_)
widget.getPropertyValue("macros").add("DI05", di05_)
widget.getPropertyValue("macros").add("DI06", di06_)
widget.getPropertyValue("macros").add("DI07", di07_)
widget.getPropertyValue("macros").add("DI08", di08_)
widget.getPropertyValue("macros").add("DI09", di09_)
widget.getPropertyValue("macros").add("DI10", di10_)
widget.getPropertyValue("macros").add("DI11", di11_)
widget.getPropertyValue("macros").add("DI12", di12_)
widget.getPropertyValue("macros").add("DI13", di13_)
widget.getPropertyValue("macros").add("DI14", di14_)
widget.getPropertyValue("macros").add("DI15", di15_)

