from org.csstudio.display.builder.runtime.script import PVUtil
from jarray import array
from time import sleep

power_meter = PVUtil.getString(pvs[0]) #power_meter

if power_meter == "NRP2 105996":
    pv_PM_choice_P = PVUtil.createPV("loc://P_PM", 0)
    pv_PM_choice_P.write("rfcalib:")
    PVUtil.releasePV(pv_PM_choice_P)

    pv_PM_choice_R = PVUtil.createPV("loc://R_PM", 0)
    pv_PM_choice_R.write("RFS-NRP-01:")
    PVUtil.releasePV(pv_PM_choice_R)

    widget.getPropertyValue("macros").add("P_PM", "rfcalib:")
    widget.getPropertyValue("macros").add("R_PM", "RFS-NRP-01:")
    
elif power_meter == "NRP2 105997":
    pv_PM_choice_P = PVUtil.createPV("loc://P_PM", 0)
    pv_PM_choice_P.write("rfcalib:")
    PVUtil.releasePV(pv_PM_choice_P)

    pv_PM_choice_R = PVUtil.createPV("loc://R_PM", 0)
    pv_PM_choice_R.write("RFS-NRP-02:")
    PVUtil.releasePV(pv_PM_choice_R)

    widget.getPropertyValue("macros").add("P_PM", "rfcalib:")
    widget.getPropertyValue("macros").add("R_PM", "RFS-NRP-02:")
    
elif power_meter == "NRPZ Hub":
    pv_PM_choice_P = PVUtil.createPV("loc://P_PM", 0) #TS2-RF:RS_NRPZ-01-Ch0:
    pv_PM_choice_P.write("rfcalib:")
    PVUtil.releasePV(pv_PM_choice_P)

    pv_PM_choice_R = PVUtil.createPV("loc://R_PM", 0)
    pv_PM_choice_R.write("RS_NRPZ-01-Ch0:")
    PVUtil.releasePV(pv_PM_choice_R)

    widget.getPropertyValue("macros").add("P_PM", "rfcalib:")
    widget.getPropertyValue("macros").add("R_PM", "RS_NRPZ-01-Ch0:")
